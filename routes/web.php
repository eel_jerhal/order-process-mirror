<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('menuPrincipal');
});
//PEDIDOS
Route::resource('/pedidos', 'PedidosController');
//CALIFICACIÓN
Route::resource('/calificacion', 'CalificacionController');
//CATEGORIAS
Route::get('/pedidos/get_categorias/{id}', 'CategoriasController@getCategorias');
//PRODUCTOS
Route::get('/pedidos/get_productos/{id}', 'ProductosController@getProductos');
//PEDIDO PRODUCTO
Route::get('/pedidos/get_pedidoProductos/{id}', 'PedidoProductoController@getPedidoProductos');
