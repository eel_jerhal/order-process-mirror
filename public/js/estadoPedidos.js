$(document).ready(function(){
    $("#pedidosTable").on('click','.table-btn-ver',function(){
        var icon = $(this).children("i");
        var fila=$(this).closest("tr");
        var id_pedido=fila.find("td:eq(0)").text();

        if($("#pedido" + id_pedido).hasClass("empty")){
            $.ajax({
                url: '/pedidos/get_pedidoProductos/' + id_pedido,
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader' + id_pedido).toggleClass('d-none');
                },
                success:function(data) {
                    $.each(data, function(key, value){
                        $("#productos" + id_pedido).append('<li>'+ value.nombre_prod + ' x'+ value.cantidad +'</li>');
                    });
                },
                complete:function() {
                    $('#loader' + id_pedido).toggleClass('d-none');
                }
            });
            $("#pedido" + id_pedido).removeClass('empty');
            icon.removeClass("fa-eye");
            icon.addClass("fa-eye-slash");
        }else{
            if (icon.hasClass("fa-eye")) {
                icon.removeClass("fa-eye");
                icon.addClass("fa-eye-slash");
                $("#pedido" + id_pedido).toggleClass("d-none");
            }else{
                icon.removeClass("fa-eye-slash");
                icon.addClass("fa-eye");
                $("#pedido" + id_pedido).toggleClass("d-none");
            }
        }
   });

   $("#pedidosTable").on('click','.table-btn-ocultar',function(){
        $(this).closest(".table-btn-ver").toggleClass('d-none');
    });

});
