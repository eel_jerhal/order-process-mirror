$(document).ready(function(){

    //evento click para los platos
    $("#chkPlatos").click(function(){
        $("#formPlatos").toggleClass("d-none");
        var tipoId = $(this).val();
        cargarCategorias(tipoId, "Plato");
    });
    //evento click para los bebestibles
    $("#chkBebestibles").click(function(){
        $("#formBebestibles").toggleClass("d-none");
        var tipoId = $(this).val();
        cargarCategorias(tipoId, "Bebestible");
    });
    //evento click para las tablas
    $("#chkTablas").click(function(){
        $("#formTablas").toggleClass("d-none");
        var tipoId = $(this).val();
        cargarCategorias(tipoId, "Tabla");
    });
    //evento click para las salsas
    $("#chkSalsas").click(function(){
        $("#formSalsas").toggleClass("d-none");
        var tipoId = $(this).val();
        cargarCategorias(tipoId, "Salsa");
    });
    //evento click para otros
    $("#chkOtros").click(function(){
        //$("#formOtros").toggleClass("d-none");
    });

    //Evento de cambio en el selector de categorías de los platos
    $('select[name="categoriaPlato"]').on('change', function(){
        cateId = $(this).val();
        cargarProductos(cateId, "plato");
    });
    //Evento de cambio en el selector de categorías de los bebestibles
    $('select[name="categoriaBebestible"]').on('change', function(){
        cateId = $(this).val();
        cargarProductos(cateId, "bebestible");
    });
    //Evento de cambio en el selector de categorías de los tablas
    $('select[name="categoriaTabla"]').on('change', function(){
        cateId = $(this).val();
        cargarProductos(cateId, "tabla");
    });
    //Evento de cambio en el selector de categorías de los salsas
    $('select[name="categoriaSalsa"]').on('change', function(){
        cateId = $(this).val();
        cargarProductos(cateId, "salsa");
    });

    //Función que carga las categorías correspondientes al tipo desde la BD
    function cargarCategorias(tipoId, tipo){
        $.ajax({
            url: '/pedidos/get_categorias/' + tipoId,
            type:"GET",
            dataType:"json",
            beforeSend: function(){
                $('#loaderCategorias'+ tipo).toggleClass('d-none');
            },
            success:function(data) {
                $('select[name="categoria'+ tipo +'"]').empty();
                $.each(data, function(key, value){
                    $('select[name="categoria'+ tipo +'"]').append('<option value="'+ key +'">' + value + '</option>');
                });
            },
            complete: function(){
                $('#loaderCategorias'+ tipo).toggleClass('d-none');
                //Se llama a cargar los productos de la categoria
                cargarProductos($('#categoria'+ tipo +'').val(), tipo.toLowerCase());
            }
        });
    }

    //Función que carga los productos correspondientes a la categoría
    function cargarProductos(cateId, tipo){
        $.ajax({
            url: '/pedidos/get_productos/' + cateId,
            type:"GET",
            dataType:"json",
            beforeSend: function(){
                $('#loader-'+ tipo).toggleClass('d-none');
            },
            success:function(data) {
                $('select[name="' + tipo + '"]').empty();
                $.each(data, function(key, value){
                    $('select[name="' + tipo + '"]').append('<option value="'+ key +'">' + value + '</option>');
                });
            },
            complete: function() {
                $('#loader-'+ tipo).toggleClass('d-none');
            }
        });
    }
});
