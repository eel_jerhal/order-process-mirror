<?php

use Illuminate\Database\Seeder;

class CategoriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorias')->insert([
            [//1
                'nombre_cate' => "Pastas",
                'id_tipo' => "1",
            ],
            [//2
                'nombre_cate' => "Sopas",
                'id_tipo' => "1",
            ],
            [//3
                'nombre_cate' => "Bebidas",
                'id_tipo' => "2",
            ],
            [//4
                'nombre_cate' => "Jugos",
                'id_tipo' => "2",
            ],
            [//5
                'nombre_cate' => "Ensaladas",
                'id_tipo' => "1",
            ],
            [//6
                'nombre_cate' => "Picantes",
                'id_tipo' => "4",
            ],
            [//7
                'nombre_cate' => "Dulces",
                'id_tipo' => "4",
            ],
            [//8
                'nombre_cate' => "Queso",
                'id_tipo' => "4",
            ],
            [//9
                'nombre_cate' => "Tabla Caliente",
                'id_tipo' => "3",
            ],
            [//10
                'nombre_cate' => "Tabla Fría",
                'id_tipo' => "3",
            ],
            [//11
                'nombre_cate' => "Snacks",
                'id_tipo' => "3",
            ]
        ]);
    }
}
