<?php

use Illuminate\Database\Seeder;

class ProductoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productos')->insert([
            [//1
                'id_cate' => "1",
                'nombre_prod' => "Lasagna",
                'precio' => "3500",
            ],
            [//2
                'id_cate' => "1",
                'nombre_prod' => "Ravioles Espinaca Carne",
                'precio' => "4000",
            ],
            [//3
                'id_cate' => "1",
                'nombre_prod' => "Panzotti Espinaca Ricota",
                'precio' => "5000",
            ],
            [//4
                'id_cate' => "2",
                'nombre_prod' => "Crema de Espárragos",
                'precio' => "3200",
            ],
            [//5
                'id_cate' => "2",
                'nombre_prod' => "Casuela de Vacuno",
                'precio' => "3400",
            ],
            [//6
                'id_cate' => "2",
                'nombre_prod' => "Crema de Brócoli",
                'precio' => "3200",
            ],
            [//7
                'id_cate' => "3",
                'nombre_prod' => "Fanta 500cc",
                'precio' => "1200",
            ],
            [//8
                'id_cate' => "3",
                'nombre_prod' => "Sprite 500cc",
                'precio' => "1200",
            ],
            [//9
                'id_cate' => "3",
                'nombre_prod' => "Coca-Cola 500cc",
                'precio' => "1200",
            ],
            [//10
                'id_cate' => "4",
                'nombre_prod' => "Natural de Naranja",
                'precio' => "1500",
            ],
            [//11
                'id_cate' => "4",
                'nombre_prod' => "Natural de Frutilla",
                'precio' => "1500",
            ],
            [//12
                'id_cate' => "4",
                'nombre_prod' => "Watts de Manzana 500cc",
                'precio' => "800",
            ],
            [//13
                'id_cate' => "5",
                'nombre_prod' => "Ensalada a la Chilena",
                'precio' => "1000",
            ],
            [//14
                'id_cate' => "5",
                'nombre_prod' => "Choclo",
                'precio' => "1000",
            ],
            [//15
                'id_cate' => "5",
                'nombre_prod' => "Pepino",
                'precio' => "1000",
            ],
            [//16
                'id_cate' => "6",
                'nombre_prod' => "Mayo Merkén",
                'precio' => "600",
            ],
            [//17
                'id_cate' => "6",
                'nombre_prod' => "Pebre",
                'precio' => "800",
            ],
            [//18
                'id_cate' => "7",
                'nombre_prod' => "Manjar",
                'precio' => "500",
            ],
            [//19
                'id_cate' => "7",
                'nombre_prod' => "Leche Condensada",
                'precio' => "700",
            ],
            [//20
                'id_cate' => "8",
                'nombre_prod' => "Queso Derretido",
                'precio' => "1000",
            ],
            [//21
                'id_cate' => "8",
                'nombre_prod' => "Salsa de Queso",
                'precio' => "700",
            ],
            [//22
                'id_cate' => "9",
                'nombre_prod' => "Trozos variados de lomo",
                'precio' => "4000",
            ],
            [//23
                'id_cate' => "9",
                'nombre_prod' => "Costillar",
                'precio' => "6000",
            ],
            [//24
                'id_cate' => "10",
                'nombre_prod' => "Trozos de Quesos, Fiambres y Otros",
                'precio' => "1500",
            ],
            [//25
                'id_cate' => "10",
                'nombre_prod' => "Sushi",
                'precio' => "3000",
            ],
            [//26
                'id_cate' => "11",
                'nombre_prod' => "Snacks Varios",
                'precio' => "1000",
            ],
            [//26
                'id_cate' => "11",
                'nombre_prod' => "Galletas dulces de Chocolate",
                'precio' => "1000",
            ],
        ]);
    }
}
