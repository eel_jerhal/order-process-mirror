<?php

use Illuminate\Database\Seeder;

class TipoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos')->insert([
            [//1
                'nombre_tipo' => "Platos",
            ],
            [//2
                'nombre_tipo' => "Bebestibles",
            ],
            [//3
                'nombre_tipo' => "Tablas",
            ],
            [//4
                'nombre_tipo' => "Salsas",
            ],
            [//5
                'nombre_tipo' => "Otros",
            ]
        ]);
    }
}
