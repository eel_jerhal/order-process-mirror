<?php

use Illuminate\Database\Seeder;

class MesaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mesas')->insert([
            [//1
                'estado_mesa'   => "0",
                'cant_asientos' => "4",
            ],
            [//2
                'estado_mesa'   => "1",
                'cant_asientos' => "2",
            ],
            [//3
                'estado_mesa'   => "1",
                'cant_asientos' => "4",
            ],
            [//4
                'estado_mesa'   => "0",
                'cant_asientos' => "6",
            ],
            [//5
                'estado_mesa'   => "0",
                'cant_asientos' => "2",
            ],
            [//6
                'estado_mesa'   => "0",
                'cant_asientos' => "4",
            ]
        ]);
    }
}
