<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>
        <link rel="shortcut icon" href="{{asset('img/logos/orderprocess.ico')}}" />
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <link rel="stylesheet" href="{{asset('css/general.css')}}">
    </head>
    <body class="body-color">
        <div class="container py-5">
            <div class="row justify-content-center">
                <div class="col-7">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <div class="alert alert-{{ $msg }} alert-dismissible fade show">{{ Session::get('alert-' . $msg) }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    @endforeach
                </div>
                <div class="col-sm-12">
                    <h2 class="text-center text-white mb-4">Ingrese Orden</h2>
                    <div class="row">
                        <div class="col-md-7 mx-auto">
                            <div class="card card-color">
                                <div class="card-body">
                                    <form id="add-pedido" name="pedido" method="POST" action="{{ route('pedidos.store') }}">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label class="text-light" for="SeleccionMesa">Mesa Nro:</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <select class="form-control boxed" name="num_mesa" id="mesa" title="Selección de Mesa">
                                                        @foreach ($mesas as $mesa)
                                                            <option value="{{ $mesa->num_mesa }}"> {{ $mesa->num_mesa }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- Para cargar los tipos dinamicamente --}}
                                        {{-- <div class="form-group">
                                            @foreach ($tipos as $tipo)
                                                <div class="form-check" id="sec{{ $tipo->nombre_tipo }}">
                                                    <input class="form-check-input" type="checkbox" value="" id="chk{{ $tipo->nombre_tipo }}">
                                                    <label class="form-check-label text-light" for="chk{{ $tipo->nombre_tipo }}">
                                                        {{ $tipo->nombre_tipo }}
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div> --}}
                                        <div class="form-group">
                                            {{-- CheckBox Platos --}}
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="1" id="chkPlatos" name="chkPlatos">
                                                <label class="form-check-label text-light" for="chkPlatos">
                                                    Platos
                                                </label>
                                            </div>
                                            {{-- /CheckBox Platos --}}
                                            <!-- Form Platos -->
                                            <div class="row mt-2 d-none" id="formPlatos">
                                                <div class="card col-11 mx-auto d-block card-color-darker">
                                                    <div class="card-body" id="cardPlatos">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <button id="btnAgregar" type="button"
                                                                    class="btn btn-success float-left disabled">Agregar</button>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <button id="btnQuitar" type="button"
                                                                    class="btn btn-danger float-right d-none">Quitar</button>
                                                            </div>
                                                        </div>
                                                        <div class="row mt-3">
                                                            <div class="col-sm-5">
                                                                <label class="text-light"
                                                                    for="selectCategoriaPlato">Seleccione
                                                                    Categoría:</label>
                                                            </div>
                                                            <div class="col-sm-1"><span id="loaderCategoriasPlato" class="text-light d-none"><i class="fas fa-spinner fa-pulse"></i></span></div>
                                                            <div class="col-sm-6">
                                                                <select class="form-control boxed" name="categoriaPlato" id="categoriaPlato" title="Selección de Categoría"></select>
                                                            </div>
                                                        </div>
                                                        <div class="row mt-3">
                                                            <div class="text-light col-sm-4">
                                                                <label class="text-light" for="selectPlato">Seleccione
                                                                    Plato/Cant:</label>
                                                            </div>
                                                            <div class="col-sm-1"><span id="loader-plato" class="text-light d-none"><i class="fas fa-spinner fa-pulse"></i></span></div>
                                                            <div class="col-sm-5">
                                                                <select class="form-control boxed" name="plato" id="plato" title="Selección de Plato"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <input class="form-control" name="cantPlatos" value="1"
                                                                    id="cantPlatos" type="number" title="Cantidad de Platos">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /Form Platos -->
                                            {{-- CheckBox Bebestibles --}}
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="2" id="chkBebestibles" name="chkBebestibles">
                                                <label class="form-check-label text-light" for="chkBebestibles">
                                                    Bebestibles
                                                </label>
                                            </div>
                                            {{-- /CheckBox Bebestibles --}}
                                            <!-- Form Bebestibles -->
                                            <div class="row mt-2 d-none" id="formBebestibles">
                                                <div class="card col-11 mx-auto d-block card-color-darker">
                                                    <div class="card-body" id="cardBebestibles">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <button id="btnAgregar" type="button"
                                                                    class="btn btn-success float-left disabled">Agregar</button>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <button id="btnQuitar" type="button"
                                                                    class="btn btn-danger float-right d-none">Quitar</button>
                                                            </div>
                                                        </div>
                                                        <div class="row mt-3">
                                                            <div class="col-sm-5">
                                                                <label class="text-light"
                                                                    for="selectCategoriaBebestible">Seleccione
                                                                    Categoría:</label>
                                                            </div>
                                                            <div class="col-sm-1"><span id="loaderCategoriasBebestible" class="text-light d-none"><i class="fas fa-spinner fa-pulse"></i></span></div>
                                                            <div class="col-sm-6">
                                                                <select class="form-control boxed" name="categoriaBebestible" id="categoriaBebestible" title="Selección de Categoría"></select>
                                                            </div>
                                                        </div>
                                                        <div class="row mt-3">
                                                            <div class="text-light col-sm-4">
                                                                <label class="text-light" for="selectBebestible">Seleccione
                                                                    Bebestible/Cant:</label>
                                                            </div>
                                                            <div class="col-sm-1"><span id="loader-bebestible" class="text-light d-none"><i class="fas fa-spinner fa-pulse"></i></span></div>
                                                            <div class="col-sm-5">
                                                                <select class="form-control boxed" name="bebestible" id="bebestible" title="Selección de Bebestible"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <input class="form-control" name="cantBebestibles" value="1"
                                                                    id="cantBebestibles" type="number" title="Cantidad de Bebestibles">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /Form Bebestibles -->
                                            {{-- CheckBox Tablas --}}
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="3" id="chkTablas" name="chkTablas">
                                                <label class="form-check-label text-light" for="chkTablas">
                                                    Tablas
                                                </label>
                                            </div>
                                            {{-- /CheckBox Tablas --}}
                                            <!-- Form Tablas -->
                                            <div class="row mt-2 d-none" id="formTablas">
                                                <div class="card col-11 mx-auto d-block card-color-darker">
                                                    <div class="card-body" id="cardTablas">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <button id="btnAgregar" type="button"
                                                                    class="btn btn-success float-left disabled">Agregar</button>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <button id="btnQuitar" type="button"
                                                                    class="btn btn-danger float-right d-none">Quitar</button>
                                                            </div>
                                                        </div>
                                                        <div class="row mt-3">
                                                            <div class="col-sm-5">
                                                                <label class="text-light"
                                                                    for="selectCategoriaTabla">Seleccione
                                                                    Categoría:</label>
                                                            </div>
                                                            <div class="col-sm-1"><span id="loaderCategoriasTabla" class="text-light d-none"><i class="fas fa-spinner fa-pulse"></i></span></div>
                                                            <div class="col-sm-6">
                                                                <select class="form-control boxed" name="categoriaTabla" id="categoriaTabla" title="Selección de Categoría"></select>
                                                            </div>
                                                        </div>
                                                        <div class="row mt-3">
                                                            <div class="text-light col-sm-4">
                                                                <label class="text-light" for="selectTabla">Seleccione
                                                                    Tabla/Cant:</label>
                                                            </div>
                                                            <div class="col-sm-1"><span id="loader-tabla" class="text-light d-none"><i class="fas fa-spinner fa-pulse"></i></span></div>
                                                            <div class="col-sm-5">
                                                                <select class="form-control boxed" name="tabla" id="tabla" title="Selección de Tabla"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <input class="form-control" name="cantTablas" value="1"
                                                                    id="cantTablas" type="number" title="Cantidad de Tablas">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /Form Tablas -->
                                            {{-- CheckBox Salsas --}}
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="4" id="chkSalsas" name="chkSalsas">
                                                <label class="form-check-label text-light" for="chkSalsas">
                                                    Salsas
                                                </label>
                                            </div>
                                            {{-- /CheckBox Salsas --}}
                                            <!-- Form Salsas -->
                                            <div class="row mt-2 d-none" id="formSalsas">
                                                <div class="card col-11 mx-auto d-block card-color-darker">
                                                    <div class="card-body" id="cardSalsas">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <button id="btnAgregar" type="button"
                                                                    class="btn btn-success float-left disabled">Agregar</button>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <button id="btnQuitar" type="button"
                                                                    class="btn btn-danger float-right d-none">Quitar</button>
                                                            </div>
                                                        </div>
                                                        <div class="row mt-3">
                                                            <div class="col-sm-5">
                                                                <label class="text-light"
                                                                    for="selectCategoriaSalsa">Seleccione
                                                                    Categoría:</label>
                                                            </div>
                                                            <div class="col-sm-1"><span id="loaderCategoriasSalsa" class="text-light d-none"><i class="fas fa-spinner fa-pulse"></i></span></div>
                                                            <div class="col-sm-6">
                                                                <select class="form-control boxed" name="categoriaSalsa" id="categoriaSalsa" title="Selección de Categoría"></select>
                                                            </div>
                                                        </div>
                                                        <div class="row mt-3">
                                                            <div class="text-light col-sm-4">
                                                                <label class="text-light" for="selectSalsa">Seleccione
                                                                    Salsa/Cant:</label>
                                                            </div>
                                                            <div class="col-sm-1"><span id="loader-salsa" class="text-light d-none"><i class="fas fa-spinner fa-pulse"></i></span></div>
                                                            <div class="col-sm-5">
                                                                <select class="form-control boxed" name="salsa" id="salsa" title="Selección de Salsa"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <input class="form-control" name="cantSalsas" value="1"
                                                                    id="cantSalsas" type="number" title="Cantidad de Salsas">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /Form Tablas -->
                                            <div class="row mt-4">
                                                <div class="col-sm-6">
                                                    <!-- <button type="button" class="btn btn-warning btn-lg float-left" id="btnVolver">Volver</button> -->
                                                    <a href="{{ url('/') }}" class="btn btn-warning btn-lg float-left">Volver</a>
                                                </div>
                                                <div class="col-sm-6">
                                                    <button type="button" class="btn btn-success btn-lg float-right" id="btnOrden" data-toggle="modal" data-target="#guardarModal">Realizar Orden</button>
                                                    {{-- <a href="#" class="btn btn-success btn-lg float-right">Realizar Orden</a> --}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal fade" id="guardarModal" tabindex="-1" role="dialog" aria-labelledby="confirmacionModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="confirmacionModalLabel">Confirmación</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Quiere guardar el pedido?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>
                                                        <button type="submit" class="btn btn-primary">Realizar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /CardBody -->
                            </div>
                            <!-- /Card -->
                        </div>
                        <!-- /Col -->
                    </div>
                    <!-- /Row -->
                </div>
                <!-- /Col -->
            </div>
            <!-- /Row -->
        </div>
        <!--/container-->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="{{ asset('js/pedidos.js') }}"></script>
    </body>
</html>
