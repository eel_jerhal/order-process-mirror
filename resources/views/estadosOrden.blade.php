<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>
        <link rel="shortcut icon" href="{{asset('img/logos/orderprocess.ico')}}" />
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="{{asset('css/general.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
        <script src="{{ asset('js/estadoPedidos.js') }}"></script>
    </head>
    <body class="body-color">
            <div class="container py-5">
                <div class="row">
                    <div class="col-sm-12 table-responsive">
                        <h2 class="text-center text-white mb-5 mt-3">Estado de las Ordenes</h2>
                        <table class="table table-striped table-dark table-bordered" id="pedidosTable">
                            <thead>
                                <tr>
                                    <th scope="col" rowspan="1">#</th>
                                    <th scope="col" rowspan="1">Hora</th>
                                    <th scope="col" rowspan="1">Mesa</th>
                                    <th scope="col" rowspan="1">Estado</th>
                                    <th scope="col" colspan="2">Productos</th>
                                    <th scope="col" rowspan="1">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pedidos as $pedido)
                                    <tr>
                                        <td>{{$pedido->id_pedido}}</td>
                                        <td>{{$pedido->hora_pedido}}</td>
                                        <td>{{$pedido->num_mesa}}</td>
                                        <td>
                                            @switch($pedido->estado)
                                                @case(0)
                                                    <div class="text-warning">
                                                        En Espera
                                                    </div>
                                                    @break
                                                @case(1)
                                                    <div class="text-info">
                                                        Preparando
                                                    </div>
                                                    @break
                                                @case(2)
                                                    <div class="text-success">
                                                        Listo
                                                    </div>
                                                    @break
                                            @endswitch
                                        </td>
                                        <td>
                                            <div class="col empty" id="pedido{{$pedido->id_pedido}}">
                                                <ul id="productos{{$pedido->id_pedido}}">
                                                    <div class="col-sm-1"><span id="loader{{$pedido->id_pedido}}" class="text-light d-none"><i class="fas fa-spinner fa-pulse"></i></span></div>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col">
                                                <div class= "row d-flex justify-content-center">
                                                    <button type="button" class="table-btn-ver btn btn-info" title="Cargar/Ocultar Productos">
                                                        <i class="fas fa-eye"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class= "row d-flex justify-content-center">
                                                <button type="button" class="table-btn-editar btn btn-success" title="Editar Pedido">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col">
                                <a class="btn btn-warning float-right" href="{{ url('/') }}">Volver</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    </body>
</html>
