<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/general.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/star-rating-svg.css')}}">
    <link rel="stylesheet" href="{{asset('css/modal.css')}}">
    <title>Clasificación</title>
</head>

<body class="body-color">
    <div class="container">
        <div class="row">
            <div class="col mb-3 mt-5">
                <img class="mx-auto d-block img-fluid" src="{{asset('img/iconos/reward.png')}}" width="20%" height="20%">
            </div>
        </div>
        <div class="row">
            <div class="col mt-2">
                <h3 class="text-center text-light">Gracias por su preferencia, sea libre de calificar nuestro
                    servicio.</h3>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col mt-5">
                    <span class="calificacion d-flex justify-content-center"></span>
                    <h3 class="live-rating text-light"></h3>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="clasificacionModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="col pt-3">
                        <h5 class="text-light text-center">Muchas gracias por su calificación, esperamos verte pronto ;)</h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{ url('/') }}" class="btn btn-success">Cerrar</a>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script src="{{asset('js/jquery.star-rating-svg.min.js')}}"></script>
    <script src="{{asset('js/calificacion.js')}}"></script>
</body>

</html>
