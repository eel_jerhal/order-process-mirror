<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;


class PedidoProducto extends Model
{
    use HasCompositePrimaryKey;
    protected $table = "pedido_productos";
    protected $primaryKey = array('id_pedido', 'id_prod');
    public $timestamps = false;


    protected $fillable = [
        'id_pedido', 'id_prod', 'cantidad',
    ];

    public function producto()
    {
    	return $this->belongsTo('\App\Producto');
    }
    public function pedido()
    {
    	return $this->belongsTo('\App\Pedido');
    }
}
