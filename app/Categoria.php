<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = "categorias";
    protected $primaryKey = 'id_cate';
    public $timestamps = false;

    protected $fillable = [
        'nombre_cate', 'id_tipo',
    ];

    public function tipo()
    {
    	return $this->belongsTo('\App\Tipo');
    }
    public function productos()
    {
    	return $this->hasMany('\App\Producto');
    }
}
