<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mesa extends Model
{
    protected $table = "mesas";
    protected $primaryKey = 'num_mesa';
    public $timestamps = false;

    protected $fillable = [
        'estado_mesa', 'cant_asientos',
    ];

    public function pedidos()
    {
    	return $this->hasOne('\App\Pedido');
    }
}
