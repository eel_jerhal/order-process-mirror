<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PedidoProducto;

class PedidoProductoController extends Controller
{
    public function getPedidoProductos($id_pedido) {
        $productos = PedidoProducto::join('productos', 'pedido_productos.id_prod', '=', 'productos.id_prod')
        ->select('productos.nombre_prod', 'pedido_productos.cantidad')
        ->where('pedido_productos.id_pedido',$id_pedido)->get();
        return json_encode($productos);
    }
}
