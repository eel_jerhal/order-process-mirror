<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Mesa;
use App\PedidoProducto;
use \App\Pedido;
use Illuminate\Support\Carbon;

class PedidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pedidos = Pedido::orderBy('hora_pedido', 'ASC')->get();
        return View('estadosOrden', compact('pedidos'));
        /*
        $pedidoProductos = array();
        foreach ($pedidos as $key => $value) {
            $productos[] = PedidoProducto::join('productos', 'pedido_productos.id_prod', '=', 'productos.id_prod')
            ->select('productos.nombre_prod', 'pedido_productos.cantidad')
            ->where('pedido_productos.id_pedido',$value->id_pedido)->get();

            $pedidoProducto[] = array('id_pedido' => $value->id_pedido, 'productos' => $productos);
        }
        return View('estadosOrden', compact('pedidoProducto')); */
        /* dd($pedidoProducto); */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /* $mesas = Mesa::where('estado_mesa', 0)->get(); */
        $mesas = Mesa::all();
        return View('ingresarPedido', compact('mesas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pedido = new Pedido();
        $hora = Carbon::now();

        $pedido->num_mesa = $request->num_mesa;
        $pedido->estado_pedido = "0";
        $pedido->hora_pedido = $hora->toTimeString();
        $pedido->save();
        //TODO: Modificar para que se haga de forma dinámica.
        $cont = 0;
        if ($request->chkPlatos) {
            $pedidoProducto = new PedidoProducto();
            $pedidoProducto->id_pedido = $pedido->id_pedido;
            $pedidoProducto->id_prod = $request->plato;
            $pedidoProducto->cantidad = $request->cantPlatos;
            $pedidoProducto->save();
            $cont++;
        }
        if ($request->chkBebestibles) {
            $pedidoProducto = new PedidoProducto();
            $pedidoProducto->id_pedido = $pedido->id_pedido;
            $pedidoProducto->id_prod = $request->bebestible;
            $pedidoProducto->cantidad = $request->cantBebestibles;
            $pedidoProducto->save();
            $cont++;
        }
        if ($request->chkTablas) {
            $pedidoProducto = new PedidoProducto();
            $pedidoProducto->id_pedido = $pedido->id_pedido;
            $pedidoProducto->id_prod = $request->tabla;
            $pedidoProducto->cantidad = $request->cantTablas;
            $pedidoProducto->save();
            $cont++;
        }
        if ($request->chkSalsas) {
            $pedidoProducto = new PedidoProducto();
            $pedidoProducto->id_pedido = $pedido->id_pedido;
            $pedidoProducto->id_prod = $request->salsa;
            $pedidoProducto->cantidad = $request->cantSalsas;
            $pedidoProducto->save();
            $cont++;
        }
        if ($cont == 0) {
            $pedido = Pedido::findOrFail($pedido->id_pedido);
            $pedido->delete();
            $request->session()->flash('alert-danger', 'Ingrese valores para guardar el pedido!');
            return redirect()->back();
        }
        $request->session()->flash('alert-success', 'Producto Ingresado con Exito!');
        return redirect('/');
        /* return view('menuPrincipal'); */
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
