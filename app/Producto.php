<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = "productos";
    protected $primaryKey = 'id_prod';
    public $timestamps = false;


    protected $fillable = [
        'id_cate', 'nombre_prod', 'precio',
    ];

    public function categoria()
    {
    	return $this->belongsTo('\App\Categoria');
    }
    public function pedido_productos()
    {
    	return $this->hasMany('\App\PedidoProducto');
    }
}
