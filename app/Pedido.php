<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $table = "pedidos";
    protected $primaryKey = 'id_pedido';
    public $timestamps = false;

    protected $fillable = [
        'num_mesa', 'estado_pedido', 'hora_pedido', 'hora_servicio',
    ];

    public function mesa()
    {
    	return $this->belongsTo('\App\Mesa');
    }

    public function pedidos_producto()
    {
    	return $this->hasMany('\App\PedidoProducto');
    }
}
